const fs = require("fs");
const path = require("path");

const lipsumPath = path.join(__dirname, "./lipsum.txt");
const filenamesPath = path.join(__dirname, "./filenames.txt");
const upperTxtPath = path.join(__dirname, "./upper.txt");
const lowerSentencePath = path.join(__dirname, "./lowerSentence.txt");
const sortedLowerCaseSentence = path.join(__dirname, "sortedSentences.txt");

function convertToUpperCaseAndWriteTONewFile(data, callbacks) {
  const upperCaseContent = data.toUpperCase();
  fs.writeFile(upperTxtPath, upperCaseContent, function (error) {
    if (error) {
      console.log(`ERROR :  ${error} at ${upperTxtPath} `);
    } else {
      console.log(`SUCCESS : sucess in writing the file : ${upperTxtPath}`);
      fs.writeFile(filenamesPath, "upper.txt", function (error) {
        if (error) {
          console.log(`ERROR :  ${error} `);
        } else {
          console.log(`SUCCESS : append successfull in  ${filenamesPath} `);
        }
      });
    }
    callbacks();
  });
}

function readNewFileConvertToLowerCase(callback) {
  const dataFromUpperTxt = fs.readFile(upperTxtPath, "utf8", function (error, dataFromUpperTxt) {
    if (error) {
      console.log(`ERROR :  ${error} `);
    } else {
      const lowerCaseData = dataFromUpperTxt.toLowerCase();
      console.log(`SUCCESS : conversion of ${upperTxtPath} to lowercase`);
      const sentenceData = lowerCaseData.split(".")
        .map((sentence) => {
          return sentence.trim();
        })
        .filter((sentence) => {
          return sentence !== "";
        })
        .join("\n");
      fs.writeFile(lowerSentencePath, sentenceData, function (error) {
        if (error) {
          console.log(`ERROR : ${error}`);
        } else {
          console.log(`SUCCESS : write file at ${lowerSentencePath}`);
          fs.appendFile(filenamesPath, "\nlowerSentence.txt", function (error) {
            if (error) {
              console.log(
                `Error : in the writing the file name to the ${filenamesPath}`
              );
            }
          }
          );
        }
      });
    }
    callback();
  }
  );
}

function readNewFileAndSort(callback) {
  fs.readFile(lowerSentencePath, "utf-8", function (error, data) {
    if (error) {
      console.log(`Error : ${error} \n arrise in reading the file ${lowerSentencePath}`);
    } else {
      const sortedData = data.split("\n").sort().join("\n");

      fs.writeFile(sortedLowerCaseSentence, sortedData, function (error) {
        if (error) {
          console.log(`Error : error in writing ${sortedLowerCaseSentence}`);
        } else {
          console.log(`SUCCESS : write successfull at ${sortedLowerCaseSentence}`
          );
          fs.appendFile(filenamesPath, "\nsortedSentences.txt", function (error) {
            if (error) {
              console.log(`error in appending name ${sortedLowerCaseSentence} in filenames.txt`);
            }
            callback();
          }
          );
        }
      });
    }
  });
}

function readFilesnamesAndDelete(callback) {
  const fileNames = fs.readFile(filenamesPath, "utf8", function (error, data) {
    if (error) {
      console.log("error in reading the file filenames.txt");
    } else {
      data = data.split("\n");
      data.map((currentData) => {
        const unlinkPath = path.join(__dirname, currentData);
        fs.unlink(unlinkPath, function (error) {
          if (error) {
            console.log(`error in deleting the file ${currentData}`);
          } else {
            console.log(`SUCCESS : ${currentData} delete success `);
          }
        });
      });
    }
    callback();
  });
}

function problem2() {
  fs.readFile(lipsumPath, "utf-8", function (error, data) {
    if (error) {
      console.log(`Error : error in reading ${lipsumPath} ${error}`);
    } else {
      console.log(`SUCCESS : read sucessfull ${lipsumPath}`);
      convertToUpperCaseAndWriteTONewFile(data, function (error) {
        if (error) {
          console.log(`ERROR : error ain file`);
        } else {
          readNewFileConvertToLowerCase(function (error) {
            if (error) {
              console.log(`ERROR :  ${error} `);
            } else {
              readNewFileAndSort(function (error) {
                if (error) {
                  console.log(`ERROR :  ${error} `);
                } else {
                  readFilesnamesAndDelete(function (error) {
                    if (error) {
                      console.log(`ERROR :  ${error} `);
                    }
                  });
                }
              });
            }
          });
        }
      });
    }
  });
}

module.exports = problem2;

