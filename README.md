# Fs-Callback-Project
#### Project on the Node.js fs module Asynchronous function.
## Folder structure:
    ├── problem1.js
    ├── problem2.js
    └── test
        ├── testProblem1.js
        └── testProblem2.js

## Problem1 :
Using callbacks and the fs module's asynchronous functions, do the following:
-  Create a directory of random JSON files
-  Delete those files simultaneously 

## Problem2 :
Using callbacks and the fs module's asynchronous functions, do the following:
- Read the given file lipsum.txt
- Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
- Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
- Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
- Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
