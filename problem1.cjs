const fs = require("fs");
const path = require("path");

function createDirectory(dirName, callbacks) {
  const dirPath = path.join(__dirname, dirName);
  fs.mkdir(dirPath, { recursive: true }, function (error) {
    if (error) {
      console.error(error);
    } else {
      console.log(`Directory created for the JSON files.`);
      callbacks();
    }
  });
}

function createRandomFile(randomNumber, callbacks) {
  const randomObject = {
    number1: Math.random(),
    country: "Chile",
    age: Math.random(),
  };
  let counter = 0;
  for (let index = 1; index <= randomNumber; index++) {
    const jsonPath = path.join(__dirname, `./randomJSON/random${index}.json`);
    fs.writeFile(jsonPath, JSON.stringify(randomObject), function (error) {
      if (error) {
        console.log(`ERROR : ${error}`);
        counter++;
      } else {
        console.log(`random${index}.json file created`);
        counter++;
      }
      if (counter == randomNumber) {
        callbacks();
      }
    });
  }
}

function deleteRandomFile(randomNumber, callback) {
  for (let index = 1; index <= randomNumber; index++) {
    const jsonPath = path.join(__dirname, `./randomJSON/random${index}.json`);
    fs.unlink(jsonPath, function (error) {
      if (error) {
        console.log(`ERROR : ${error}`);
      } else {
        console.log(`random${index}.json file deleted successfully`);
      }
    });
  }
  callback();
}

function createDirectoryRandomJSONAndDeleteFiles() {
  const dirName = "randomJSON";
  createDirectory(dirName, function (error) {
    if (error) {
      console.err("Directory not created");
    } else {
      const randomNumber = Math.floor((Math.random() + 1) * 20);
      createRandomFile(randomNumber, function (error) {
        if (error) {
          console.log(`ERROR : ${error}`);
        } else {
          deleteRandomFile(randomNumber, function (error) {
            if (error) {
              console.log(`ERROR : ${error}`);
            }
          });
        }
      });
    }
  });
}

module.exports = createDirectoryRandomJSONAndDeleteFiles;
